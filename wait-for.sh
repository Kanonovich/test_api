#!/bin/sh

# https://docs.docker.com/compose/startup-order/

set -e
#sleep 10

db_host="$1"
shift 1
cmd="$@"

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$db_host" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done
>&2 echo "Postgres is up - executing command"
