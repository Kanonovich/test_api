from sqlalchemy import Table, MetaData, Column, String, Integer, ForeignKey, DateTime
import datetime as dt


meta = MetaData()
users = Table(
    'users',
    meta,
    Column('id', Integer, primary_key=True),
    Column('login', String, nullable=False),
    Column('password', String(50)),
    Column('created_at', DateTime, default=dt.datetime.now()),
    Column('updated_at', DateTime, default=dt.datetime.now())
)


documents = Table(
    'documents',
    meta,
    Column('id', Integer, primary_key=True),
    Column('user_id', None, ForeignKey('users.id')),
    Column('document_name', String(50)),
    Column('body', String(50)),
    Column('created_at', DateTime, default=dt.datetime.now())
)


tags = Table(
    'tags',
    meta,
    Column('id', Integer, primary_key=True),
    Column('document_name', String(50)),
    Column('tag_name', String(50)),
    Column('created_at', DateTime, default=dt.datetime.now()),
    Column('updated_at', DateTime, default=dt.datetime.now())
)


documents_tag_relation = Table(
    'documents_tag_relation',
    meta,
    Column('id', Integer, primary_key=True),
    Column('document_id', ForeignKey('documents.id')),
    Column('tag_id', ForeignKey('tags.id'))
)