from sqlalchemy.engine.base import Engine
from sqlalchemy import and_

from doc_storage.storage.models import users, documents, tags, meta, documents_tag_relation


class DatabaseClient:
    def __init__(self, engine: Engine):
        self.engine = engine
        self._create_all_tables()

    def _create_all_tables(self):
        meta.create_all(self.engine, checkfirst=True)

    def create_user(self, login, password):
        with self.engine.connect() as conn:
            conn.execute(users.insert().values(**{'login': login, 'password': password}))
        return {'login': login, 'password': password}

    def get_all_users(self):
        with self.engine.connect() as conn:
            all_users = conn.execute(users.select()).fetchall()
        return all_users

    def delete_user(self, login, password):
        with self.engine.connect() as conn:
            conn.execute(users.delete().where(and_(users.c.login == login, users.c.password == password)))
        return {'login': login, 'password': password}

    def get_user(self, user_id):
        with self.engine.connect() as conn:
            current_user = conn.execute(users.select().where(and_(users.c.id == user_id)))
        return current_user.fetchone()

    def get_user_login(self, login, password):
        with self.engine.connect() as conn:
            conn.execute(users.select().where(and_(users.c.login == login, users.c.password == password)))
        return {'login': login}

    def get_user_password(self, login, password):
        with self.engine.connect() as conn:
            conn.execute(users.select().where(and_(users.c.login == login, users.c.password == password)))
        return {'password': password}

    def get_user_changes(self, login, password):
        with self.engine.connect() as conn:
            conn.execute(
                users.update().where(
                    users.c.login == login, users.c.password == password).values({'login': login, 'password': password}))
        return {'login': login, 'password': password}

    def create_documents(self, user_id, name, body):
        with self.engine.connect() as conn:
            conn.execute(documents.insert().values(**{'user_id': user_id, 'document_name': name, 'body': body}))
        return {'login': user_id, 'document_name': name, 'body': body}

    def get_document(self, document_name):
        with self.engine.connect() as conn:
            current_document = conn.execute(documents.select().where(and_(documents.c.document_name == document_name)))
        return current_document.fetchone()

    def get_all_user_documents(self, login):
        with self.engine.connect() as conn:
            all_documents = conn.execute(
                documents.select().select_from(documents.join(users)).where(and_(users.c.login == login)))
        return all_documents.fetchall()

    def delete_document(self, user_id, name):
        with self.engine.connect() as conn:
            conn.execute(documents.delete().where(and_(documents.c.user_id == user_id, documents.c.document_name == name)))
        return {'user_id': user_id, 'document_name': name}

    def get_all_tags_current_document(self, document_name):
        with self.engine.connect() as conn:
            all_tags = conn.execute(
                tags.select().select_from(
                    documents.join(tags, documents_tag_relation)).where(and_(documents.c.document_name == document_name)))
        return all_tags.fetchall()

    def get_all_user_tags(self, user_id):
        with self.engine.connect() as conn:
            all_user_tags = conn.execute(
                tags.select().select_from(
                    tags.join(documents, documents_tag_relation)).where(and_(documents.c.user_id == user_id)))
            return all_user_tags

    def create_tag(self, document_name, tag_name):
        with self.engine.connect() as conn:
            conn.execute(tags.insert().values(**{'document_name': document_name, 'tag_name': tag_name}))
        return {'tag_name': tag_name, 'document_name': document_name}

    def delete_tag(self, document_name, tag_name):
        with self.engine.connect() as conn:
            conn.execute(tags.delete().where(and_(
                documents.c.document_name == document_name, tags.c.tag_name == tag_name)))
        return {'document_name': document_name, 'tag_name': document_name}

    def change_tag_name(self, tag_name):
        with self.engine.connect() as conn:
            conn.execute(tags.update().where(tags.c.tag_name == tag_name).values({'tag_name': tag_name}))
        return {'tag_name': tag_name}


