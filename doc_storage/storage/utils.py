from .models import users, documents, tags, documents_tag_relation


def create_tables():
    users.create()
    documents.create()
    tags.create()
    documents_tag_relation.create()


def drop_tables():
    users.drop()
    documents.drop()
    tags.drop()
    documents_tag_relation.drop()
