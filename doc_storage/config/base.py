from sqlalchemy import create_engine

import os
import typing as t

from doc_storage.storage.database_client import DatabaseClient


POSTGRES_DB: t.Dict[str, str] = {
    'USER': os.getenv('POSTGRES_USER', 'postgres'),
    'PASSWORD': os.getenv('POSTGRES_PASSWORD', 'postgres'),
    'HOST': os.getenv('POSTGRES_HOST', 'localhost'),
    'PORT': os.getenv('POSTGRES_PORT', '5432'),
    'DATABASE': os.getenv('POSTGRES_DATABASE', 'postgres'),
}

engine = create_engine(
    f'postgresql://{POSTGRES_DB["USER"]}:{POSTGRES_DB["PASSWORD"]}@'
    f'{POSTGRES_DB["HOST"]}:{POSTGRES_DB["PORT"]}/{POSTGRES_DB["DATABASE"]}'
)

database_client = DatabaseClient(engine)
