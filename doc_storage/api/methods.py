from flask import request

from doc_storage.api.wsgi import app
from doc_storage.config.base import database_client


@app.route('/users', methods=['GET'])
def get_all_users():
    return database_client.get_all_users()


@app.route('/add_user', methods=['POST'])
def add_users():
    data = request.get_json(silent=True)
    login = data['login']
    password = data['password']

    return database_client.create_user(login, password)


@app.route('/delete_user', methods=['POST'])
def delete_user():
    data = request.get_json(silent=True)
    login = data['login']
    password = data['password']

    return database_client.delete_user(login, password)


@app.route('/user_changes', methods=['POST'])
def user_changes():
    data = request.get_json(silent=True)
    login = data['login']
    password = data['password']
    if database_client.get_user_login(login) and database_client.get_user_password(password):  # переменные внутри функции
        return database_client.get_user_changes(login, password)
    return 'Неправильный логин или пароль'


@app.route('/documents', methods=['POST'])
def get_all_user_documents():
    data = request.get_json()
    login = data['login']
    return database_client.get_all_user_documents(login)


@app.route('/add_document', methods=['POST'])
def add_document():
    data = request.get_json(silent=True)
    user_id = data['user_id']
    name = data['document_name']
    body = data['body']
    if database_client.get_user(user_id):
        return database_client.create_documents(user_id, name, body)
    return 'Создайте пользователя'


@app.route('/delete_document', methods=['POST'])
def delete_document():
    data = request.get_json(silent=True)
    user_id = data['user_id']
    name = data['document_name']
    if database_client.get_user(user_id):
        return database_client.delete_document(user_id, name)
    return 'Такого пользователя не существует'


@app.route('/document_tags', methods=['GET'])
def get_all_tags_current_document():
    data = request.get_json()
    document_name = data['document_name']
    return database_client.get_all_tags_current_document(document_name)


@app.route('/user_tags', methods=['POST'])
def get_all_users_tags():
    data = request.get_json()
    user_id = data['user_id']
    if database_client.get_user(user_id):
        return database_client.get_all_user_tags(user_id)
    return 'Такого пользователя не существует'


@app.route('/add_tag', methods=['POST'])
def add_tag():
    data = request.get_json()
    document_name = data['document_name']
    tag_name = data['tag_name']
    if database_client.get_document(document_name):
        return database_client.create_tag(document_name, tag_name)
    return 'Такого документа не существует'


@app.route('/delete_tag', methods=['POST'])
def delete_tag():
    data = request.get_json(silent=True)
    document_name = data['document_name']
    tag_name = data['tag_name']
    if database_client.get_document(document_name):
        return database_client.delete_tag(document_name, tag_name)
    return 'Такого документа не существует'


@app.route('/change_tag_name', methods=['POST'])
def get_change_tag_name():
    data = request.get_json()
    tag_name = data['tag_name']  # лень писать проверку тега
    return database_client.change_tag_name(tag_name)