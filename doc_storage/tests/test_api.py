import pytest
import json


@pytest.fixture
def mock_get_users(mocker):
    def wrap(result):
        return mocker.patch('doc_storage.config.base.database_client.get_all_users', return_value=result)
    return wrap


@pytest.fixture
def mock_get_all_user_documents(mocker):
    def wrap(result):
        return mocker.patch('doc_storage.config.base.database_client.get_all_user_documents', return_value=result)
    return wrap


@pytest.fixture
def mock_get_all_user_tags(mocker):
    def wrap(result):
        return mocker.patch('doc_storage.config.base.database_client.get_all_user_tags', return_values=result)
    return wrap


def test_not_found(client):
    resp = client.post('some_url')

    assert resp.status_code == 404


def test_success(client, mock_get_users):
    users = "['1', '2']"
    mocked_get_users = mock_get_users(users)

    resp = client.get('/users')

    assert resp.status_code == 200
    mocked_get_users.assert_called_once()
    assert resp.data.decode() == users


def test_get_user_documents(client, mock_get_all_user_documents):
    login = '3'
    mocked_get_document = mock_get_all_user_documents(login)

    resp = client.post('/documents', data=json.dumps({'login': login}), content_type='application/json')

    assert resp.status_code == 200
    mocked_get_document.assert_called_once()
    assert resp.data.decode() == login


def test_get_all_user_tags(client, mock_get_all_user_tags):
    user_id = '1'
    mocked_get_tags = mock_get_all_user_tags(user_id)

    resp = client.post('/user_tags', data=json.dumps({'user_id': user_id}), content_type='application/json')

    assert resp.status_code == 200
    mocked_get_tags.assert_called_once()
    assert resp.data.decode() == user_id
