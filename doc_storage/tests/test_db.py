import pytest
import typing as t
import datetime as dt

from doc_storage.storage.models import users, documents, tags


@pytest.fixture
def user_login():
    return 'test_login'


@pytest.fixture
def user_password():
    return 'test_password'


def get_users(dbsession):
    return dbsession.execute(users.select()).fetchall()


def get_documents(dbsession):
    return dbsession.execute(documents.select()).fetchall()


def get_tags(dbsession):
    return dbsession.execute(tags.select()).fetchall()


def get_login_and_password(user_row_db: t.Tuple[t.Union[int, str, dt.datetime]]) -> t.Tuple[str]:
    return user_row_db[1:3]


def get_name_and_body(document_row_db: t.Tuple[t.Union[int, str, dt.datetime]]) -> t.Tuple[str]:
    return document_row_db[1:4]


def get_tag_name(tag_row_db: t.Tuple[t.Union[int, str, dt.datetime]]) -> t.Tuple[str]:
    return tag_row_db[1:3]


def test_create_user(db_client, dbsession):
    login = '123'
    password = '456'

    db_client.create_user(login, password)

    assert get_login_and_password(get_users(dbsession)[0]) == (login, password)


def test_get_all_users(db_client, user_login, user_password):
    db_client.create_user(user_login, user_password)

    all_users = db_client.get_all_users()

    assert len(all_users) == 1
    assert get_login_and_password(all_users[0]) == (user_login, user_password)


def test_get_all_users_if_not_users(db_client):
    all_users = db_client.get_all_users()

    assert len(all_users) == 0


def test_create_document(db_client, dbsession):
    login = '123'
    password = '456'
    db_client.create_user(login, password)
    print(dbsession.execute(users.select().where(login == '123' and password == '456')).fetchall())

    user_id = 3
    name = 'qwe'
    body = 'qaz'

    db_client.create_documents(user_id, name, body)

    assert get_name_and_body(get_documents(dbsession)[0]) == (user_id, name, body)


def test_create_tag(db_client, dbsession):
    login = '123'
    password = '456'
    db_client.create_user(login, password)
    print(dbsession.execute(users.select().where(login == '123' and password == '456')).fetchall())

    user_id = 4
    name = 'qwe'
    body = 'qaz'

    db_client.create_documents(user_id, name, body)
    print(dbsession.execute(documents.select().where(name == 'qwe' and body == 'qaz')).fetchall())

    tag_name = 'blablabla'
    document_name = name

    db_client.create_tag(document_name, tag_name)

    assert get_tag_name(get_tags(dbsession)[0]) == (document_name, tag_name)
