import json
import typing as t

import psycopg2
import pytest
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, scoped_session, sessionmaker

from doc_storage import config as db_config
from doc_storage.storage.models import meta as metadata
from doc_storage.api.wsgi import app
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from doc_storage.storage.database_client import DatabaseClient

TEST_DB_NAME = f'test-{db_config.POSTGRES_DB["DATABASE"]}'
# подменяем в конфиге базу данных на 'test-'
db_config.POSTGRES_DB['DATABASE'] = TEST_DB_NAME


@pytest.fixture()
def db_session(engine):
    return scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine)
    )


def prepare_psycopg_connection_params() -> t.Dict[str, str]:
    conn_params = {
        'user': db_config.POSTGRES_DB['USER'],
        'password': db_config.POSTGRES_DB['PASSWORD'],
        'host': db_config.POSTGRES_DB['HOST'],
    }

    return conn_params


@pytest.fixture(scope='session')
def engine() -> Engine:
    conn = psycopg2.connect(**prepare_psycopg_connection_params())
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT) # без этой штуки не можем создать базу, так как транзакция не открыта
    cursor = conn.cursor()
    try:
        cursor.execute(
            f'CREATE DATABASE "{TEST_DB_NAME}" OWNER "{db_config.POSTGRES_DB["USER"]}";'
        )
    except psycopg2.ProgrammingError:
        cursor.execute(f'DROP DATABASE "{TEST_DB_NAME}";')
        cursor.execute(
            f'CREATE DATABASE "{TEST_DB_NAME}" OWNER "{db_config.POSTGRES_DB["USER"]}";'
        )
    cursor.close()
    conn.close()

    return create_engine(
        f'postgresql://{db_config.POSTGRES_DB["USER"]}:{db_config.POSTGRES_DB["PASSWORD"]}@'
        f'{db_config.POSTGRES_DB["HOST"]}:{db_config.POSTGRES_DB["PORT"]}/{db_config.POSTGRES_DB["DATABASE"]}'
    )


@pytest.yield_fixture(scope='session', autouse=True)
def tables(engine):
    metadata.create_all(engine)
    yield
    metadata.drop_all(engine)


@pytest.yield_fixture(autouse=True)
def clear_tables(engine):
    yield
    for model in reversed(metadata.sorted_tables):  # удаление снизу от тегов (нельзя начать с юзера)
        engine.execute(f'DELETE FROM "{model}"')


@pytest.yield_fixture()
def dbsession(engine, tables, db_session):

    """Returns an sqlalchemy session, and after the test tears down everything properly."""
    del tables

    connection = engine.connect()
    # begin the nested transaction
    transaction = connection.begin()
    # use the connection with the already started transaction
    session = Session(bind=connection)

    yield session

    session.close()
    db_session.close()

    # roll back the broader transaction
    transaction.rollback()
    # put back the connection to the connection pool
    connection.close()


@pytest.fixture
def db_client(engine):
    return DatabaseClient(engine)


@pytest.fixture
def client():
    return app.test_client()
