#!/usr/bin/env bash

cd /opt/app
set -e

clean_up () {
    ARG=$?
    echo "Removing __pycache__ and .pytest_cache"
    find . -name __pycache__ | xargs rm -rf
    find . -name .pytest_cache | xargs rm -rf
    exit ${ARG}
}

# https://unix.stackexchange.com/questions/230421/unable-to-stop-a-bash-script-with-ctrlc
interrupt () {
    trap - EXIT # restore default EXIT handler
    trap - INT # restore default INT handler
    echo "Removing __pycache__ and .pytest_cache"
    find . -name __pycache__ | xargs rm -rf
    find . -name .pytest_cache | xargs rm -rf
    kill -s INT "$$"
}

echo "Removing __pycache__ and .pytest_cache"
find . -name __pycache__ | xargs rm -rf
find . -name .pytest_cache | xargs rm -rf

trap clean_up EXIT
trap interrupt INT

sh ./wait-for.sh test_db
PYTHONPATH=${PYTHONPATH}:. py.test $@
