FROM python:3
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN apt-get update && apt-get install -y postgresql-client && rm -rf /var/lib/apt/lists/*
EXPOSE 80
ENTRYPOINT ["python3", "run.py"]
